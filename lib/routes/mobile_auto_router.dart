import 'package:auto_route/auto_route.dart';
import 'package:school/routes/mobile_auto_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends $AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: AuthRoute.page),
        AutoRoute(page: BottomNavigationRoute.page, initial: true),
      ];
}
