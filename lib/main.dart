import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:school/core/constants/app_text_const.dart';
import 'package:school/features/bottom_navigator/bottom_navigator_cubit/bottom_navigation_cubit.dart';
import 'package:school/routes/mobile_auto_router.dart';
import 'package:school/server/service_locator.dart';
import 'package:school/theme/app_colors.dart';
import 'package:school/widgets/app_scroll_behavior.dart';

final scaffoldKey = GlobalKey<ScaffoldMessengerState>();
final appRouter = sl<AppRouter>();

void main() async {
  await init();
  runApp(const Main());
}

class Main extends StatelessWidget {
  const Main({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => sl<BottomNavigationCubit>(),
        ),
      ],
      child: Builder(
        builder: (context) => MaterialApp.router(
          scrollBehavior: AppScrollBehavior(),
          scaffoldMessengerKey: scaffoldKey,
          theme: ThemeData(
            useMaterial3: true,
            colorScheme: ColorScheme.fromSeed(
              seedColor: AppColors.peach,
              // ···
              brightness: Brightness.light,
            ),
          ),
          title: AppTextConst.appTitle,
          debugShowCheckedModeBanner: false,
          routeInformationParser: appRouter.defaultRouteParser(),
          routerDelegate: AutoRouterDelegate(appRouter),
          routeInformationProvider: appRouter.routeInfoProvider(),
        ),
      ),
    );
  }
}
