import 'package:dio/dio.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:school/core/functions/push_router_func.dart';
import 'package:school/features/auth/data/models/profile_hive_model/profile_hive_model.dart';
import 'package:school/routes/mobile_auto_router.gr.dart';
import 'package:school/server/service_locator.dart';
import 'package:school/widgets/styled_toasts.dart';

class CatchException {
  CatchException({required this.message});
  final String message;

  static CatchException convertException(dynamic error) {
    if (error is DioException) {
      if (error.type == DioExceptionType.connectionTimeout) {
        return CatchException(message: 'connectionTimeout');
      } else if (error.type == DioExceptionType.receiveTimeout) {
        return CatchException(message: 'receiveTimeout');
      } else if (error.response == null) {
        return CatchException(message: 'message');
      } else if (error.response!.statusCode == 401) {
        removeToken(error.response!.data['message']);
        return CatchException(message: error.response!.data['message']);
      } else if (error.response!.statusCode == 400) {
        return CatchException(message: error.response!.data['message']);
      } else if (error.response!.statusCode == 404) {
        return CatchException(
          message:
              'Данные не могут быть отображены по какой-либо неизвестной причине. Пожалуйста обратитесь в службу тех.поддержки',
        );
      } else {
        return CatchException(message: error.response!.data['message']);
      }
    }
    if (error is CatchException) {
      return error;
    } else {
      return CatchException(message: 'message');
    }
  }
}

void removeToken(String error) async {
  final hive = sl<Box<ProfileHiveModel>>();
  await hive.clear();
  await Future.delayed(const Duration(milliseconds: 500), () {
    AppRouting.pushFunction(const AuthRoute());
  });

  AppSnackBars.showErrorSnackBar(error);
}
