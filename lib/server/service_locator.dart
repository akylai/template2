import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:school/core/constants/hive_keys.dart';
import 'package:school/core/models/country_hive_model/county_hive_model.dart';
import 'package:school/features/auth/data/models/profile_hive_model/profile_hive_model.dart';
import 'package:school/features/auth/data/repo_implements/auth_repo_impl.dart';
import 'package:school/features/auth/domain/repositories/auth_repo.dart';
import 'package:school/features/auth/domain/use_cases/auth_use_case.dart';
import 'package:school/features/auth/presentation/login_screen/cubits/auth_cubit/auth_cubit.dart';
import 'package:school/features/bottom_navigator/bottom_navigator_cubit/bottom_navigation_cubit.dart';
import 'package:school/routes/mobile_auto_router.dart';
import 'package:school/server/dio_settings.dart';

final sl = GetIt.instance;

// ignore: long-method
Future<void> init() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  /// Other Services
  sl.registerSingleton<AppRouter>(AppRouter());

  await Hive.initFlutter();
  Hive.registerAdapter(CountryHiveModelAdapter());
  Hive.registerAdapter(ProfileHiveModelAdapter());

  final box = await Hive.openBox<CountryHiveModel>(HiveKeys.cityBox);
  final profileBox = await Hive.openBox<ProfileHiveModel>(HiveKeys.profileBox);

  //////LazySingleton
  //////Singleton
  //////Factory

  sl.registerLazySingleton(() => box);
  sl.registerLazySingleton(() => profileBox);

  sl.registerLazySingleton<Dio>(() => DioSettings(profileBox: profileBox).dio);

  /// Repository
  sl.registerFactory<AuthRepo>(() => AuthRepoImpl(dio: sl()));

  /// UseCases
  sl.registerFactory<AuthUseCase>(
      () => AuthUseCase(repo: sl(), profileBox: profileBox));

  /// BLoCs / Cubits
  sl.registerLazySingleton<BottomNavigationCubit>(
      () => BottomNavigationCubit(profileBox: profileBox));
  sl.registerFactory<AuthCubit>(() => AuthCubit(useCase: sl()));
}
