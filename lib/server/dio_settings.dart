import 'package:dio/dio.dart';
import 'package:hive/hive.dart';
import 'package:school/core/constants/app_text_const.dart';
import 'package:school/core/constants/hive_keys.dart';
import 'package:school/features/auth/data/models/profile_hive_model/profile_hive_model.dart';

class DioSettings {
  DioSettings({required this.profileBox}) {
    initialSettings();
  }
  final Box<ProfileHiveModel> profileBox;

  Dio dio = Dio(
    BaseOptions(
      baseUrl: AppTextConst.baseUrl,
      responseType: ResponseType.json,
      contentType: "application/json; charset=utf-8",
      connectTimeout: const Duration(seconds: 25),
      receiveTimeout: const Duration(seconds: 25),
    ),
  );
  void initialSettings() {
    final interceptors = dio.interceptors;
    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) async {
          final token = profileBox.get(HiveKeys.profileBox)?.token ?? '';
          if (token != '') {
            options.headers['Authorization'] = 'Bearer $token';
          }

          return handler.next(options);
        },
        onResponse: (response, handler) {
          return handler.next(response);
        },
        onError: (DioException e, handler) {
          return handler.next(e);
        },
      ),
    );
    interceptors.add(
      LogInterceptor(
        request: true,
        requestBody: true,
        requestHeader: true,
        responseBody: true,
        responseHeader: true,
      ),
    );
  }
}
