class LocaleModel {
  final String title;
  final String value;
  LocaleModel({
    required this.title,
    required this.value,
  });
}

List<LocaleModel> localeList = [
  LocaleModel(
    title: 'Английский',
    value: 'en',
  ),
  LocaleModel(
    title: 'Кыргызский',
    value: 'ky',
  ),
  LocaleModel(
    title: 'Казахский',
    value: 'kk',
  ),
  LocaleModel(
    title: 'Русский',
    value: 'ru',
  ),
  LocaleModel(
    title: 'Узбекский',
    value: 'uz',
  ),
  LocaleModel(
    title: 'Украинский',
    value: 'uk',
  ),
  LocaleModel(
    title: 'Арабский',
    value: 'ar',
  ),
  LocaleModel(
    title: 'Азербайджанский',
    value: 'az',
  ),
];
