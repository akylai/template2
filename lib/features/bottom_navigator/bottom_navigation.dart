import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:school/core/images/app_img_svgs.dart';
import 'package:school/features/bottom_navigator/widgets/drawer_widget.dart';
import 'package:school/theme/app_colors.dart';
import 'package:school/widgets/custom_app_bar.dart';

List<Widget> screens = [
  const Text('1'),
  const Text('2'),
  const Text('3'),
  const Text('4'),
  const Text('5'),
];

List<String> screenTitles = [
  'Лента',
  'Рейтинг',
  'Выберите предмет', //Тесты
  'Моя история',
  'Мой профиль',
];

@RoutePage()
class BottomNavigationScreen extends StatefulWidget {
  const BottomNavigationScreen({super.key});

  @override
  State<BottomNavigationScreen> createState() => _BottomNavigationScreenState();
}

class _BottomNavigationScreenState extends State<BottomNavigationScreen> {
  int currentPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerWidget(),
      appBar: CustomAppBar(
        title: screenTitles[currentPageIndex],
        icon: Builder(
          builder: (context) => IconButton(
            icon: const Icon(Icons.menu),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
          ),
        ),
        // actions: const [
        //   // ChangeLocaleWidget(),
        //   SizedBox(width: 30),
        // ],
      ),
      backgroundColor: Colors.white,
      body: screens[currentPageIndex],
      bottomNavigationBar: NavigationBar(
        onDestinationSelected: (int index) {
          setState(() {
            currentPageIndex = index;
          });
        },
        indicatorColor: Colors.amber,
        backgroundColor: AppColors.bej,
        elevation: 0,
        selectedIndex: currentPageIndex,
        destinations: <Widget>[
          NavigationDestination(
            selectedIcon: SvgPicture.asset(AppImgSvgs.home),
            icon: SvgPicture.asset(AppImgSvgs.homeOutline),
            label: 'Лента',
          ),
          NavigationDestination(
            selectedIcon: SvgPicture.asset(AppImgSvgs.chart),
            icon: SvgPicture.asset(AppImgSvgs.chartOutline),
            label: 'Рейтинг',
          ),
          NavigationDestination(
            selectedIcon: SvgPicture.asset(AppImgSvgs.test),
            icon: SvgPicture.asset(AppImgSvgs.testOutline),
            label: 'Тесты',
          ),
          NavigationDestination(
            selectedIcon: SvgPicture.asset(AppImgSvgs.homeOutline),
            icon: SvgPicture.asset(AppImgSvgs.homeOutline),
            label: 'История',
          ),
          NavigationDestination(
            selectedIcon: SvgPicture.asset(AppImgSvgs.profile),
            icon: SvgPicture.asset(AppImgSvgs.profileOutline),
            label: 'Профиль',
          ),
        ],
      ),
    );
  }
}
