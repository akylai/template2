import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:school/core/constants/hive_keys.dart';
import 'package:school/core/functions/push_router_func.dart';
import 'package:school/features/auth/data/models/profile_hive_model/profile_hive_model.dart';
import 'package:school/routes/mobile_auto_router.gr.dart';

part 'bottom_navigation_cubit.freezed.dart';
part 'bottom_navigation_state.dart';

class BottomNavigationCubit extends Cubit<BottomNavigationState> {
  BottomNavigationCubit({required this.profileBox})
      : super(const BottomNavigationState.initial(2));

  final Box<ProfileHiveModel> profileBox;

  bool letIndexChange = true;
  int currentIndex = 2;

  getCurrentPage(int index) {
    currentIndex = index;
    final isToken = profileBox.get(HiveKeys.profileBox)?.token != null;
    if (index != 3 && index != 4) {
      letIndexChange = true;
      emit(BottomNavigationState.initial(index));
    } else {
      if (isToken) {
        letIndexChange = true;
        emit(BottomNavigationState.initial(index));
      } else {
        letIndexChange = false;
        AppRouting.pushFunction(const AuthRoute());
        // AppSnackBars.showErrorSnackBar('рыиаодыалв');
      }
    }
  }

  emitCurrentIndex() {
    emit(BottomNavigationState.initial(currentIndex));
  }
}
