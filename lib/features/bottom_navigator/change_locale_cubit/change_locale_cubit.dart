import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:school/features/bottom_navigator/models/locale_model.dart';

part 'change_locale_cubit.freezed.dart';
part 'change_locale_state.dart';

class ChangeLocaleCubit extends Cubit<ChangeLocaleState> {
  ChangeLocaleCubit() : super(const ChangeLocaleState.initial(''));

  void getLocale(String locale) {
    emit(
      ChangeLocaleState.initial(
        localeList.singleWhere((e) => e.value == locale).title,
      ),
    );
  }
}
