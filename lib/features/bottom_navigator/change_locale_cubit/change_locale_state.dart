part of 'change_locale_cubit.dart';

@freezed
class ChangeLocaleState with _$ChangeLocaleState {
  const factory ChangeLocaleState.initial(String lang) = _Initial;
}
