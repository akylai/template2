import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:school/core/constants/hive_keys.dart';
import 'package:school/core/functions/push_router_func.dart';
import 'package:school/core/images/app_img.dart';
import 'package:school/features/auth/data/models/profile_hive_model/profile_hive_model.dart';
import 'package:school/features/bottom_navigator/widgets/exit_dialog.dart';
import 'package:school/routes/mobile_auto_router.gr.dart';
import 'package:school/server/service_locator.dart';
import 'package:school/theme/app_colors.dart';
import 'package:school/widgets/spaces.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          // DrawerHeader(
          //   padding: const EdgeInsets.only(bottom: 25),
          //   decoration: const BoxDecoration(
          //     color: AppColors.mint,
          //   ),
          //   child: SizedBox(
          //     width: context.width,
          //     child: Image.asset(
          //       AppImg.trophy,
          //       fit: BoxFit.contain,
          //     ),
          //   ),
          // ),
          ListTile(
            leading: const FaIcon(FontAwesomeIcons.squarePollHorizontal),
            minLeadingWidth: 32,
            title: const Text('О проекте'),
            onTap: () {},
          ),
          Visibility(
            visible:
                sl<Box<ProfileHiveModel>>().get(HiveKeys.profileBox)?.token !=
                    null,
            child: Column(
              children: [
                ListTile(
                  leading: const FaIcon(FontAwesomeIcons.userGear),
                  minLeadingWidth: 32,
                  title: const Text('Мой профиль'),
                  onTap: () {},
                ),
                ListTile(
                  leading: const FaIcon(FontAwesomeIcons.bowlRice),
                  minLeadingWidth: 32,
                  title: const Text('Мои угощения'),
                  onTap: () {},
                ),
                ListTile(
                  leading: const FaIcon(FontAwesomeIcons.rightFromBracket),
                  minLeadingWidth: 32,
                  title: const Text('Выйти'),
                  onTap: () async {
                    await exitDialog(context);
                    if (context.mounted) {
                      Navigator.pop(context);
                    }
                  },
                ),
              ],
            ),
          ),
          Visibility(
            visible:
                sl<Box<ProfileHiveModel>>().get(HiveKeys.profileBox)?.token ==
                    null,
            child: Column(
              children: [
                ListTile(
                  leading: const FaIcon(FontAwesomeIcons.rightToBracket),
                  minLeadingWidth: 32,
                  title: const Text('Войти'),
                  onTap: () {
                    AppRouting.pushFunction(const AuthRoute());
                  },
                ),
                ListTile(
                  leading: const FaIcon(FontAwesomeIcons.rightToBracket),
                  minLeadingWidth: 32,
                  title: const Text('Зарегистрироваться'),
                  onTap: () {},
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
