import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:school/features/bottom_navigator/change_locale_cubit/change_locale_cubit.dart';
import 'package:school/features/bottom_navigator/models/locale_model.dart';

class ChangeLocaleWidget extends StatelessWidget {
  const ChangeLocaleWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChangeLocaleCubit, ChangeLocaleState>(
      builder: (context, state) {
        return PopupMenuButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          itemBuilder: (context2) => localeList
              .map<PopupMenuItem<LocaleModel>>(
                (e) => PopupMenuItem<LocaleModel>(
                  onTap: () async {},
                  value: e,
                  child: Text(e.title),
                ),
              )
              .toList(),
          child: Text(state.lang),
        );
      },
    );
  }
}
