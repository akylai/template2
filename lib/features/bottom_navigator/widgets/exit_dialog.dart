import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:school/core/functions/push_router_func.dart';
import 'package:school/core/models/country_hive_model/county_hive_model.dart';
import 'package:school/features/auth/data/models/profile_hive_model/profile_hive_model.dart';
import 'package:school/routes/mobile_auto_router.gr.dart';
import 'package:school/server/service_locator.dart';
import 'package:school/theme/app_colors.dart';
import 'package:school/widgets/custom_button.dart';
import 'package:school/widgets/spaces.dart';

Future<void> exitDialog(BuildContext mainContext) async {
  await showDialog(
    context: mainContext,
    builder: (context) => Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: SizedBox(
        width: context.width,
        child: AlertDialog(
          insetPadding: EdgeInsets.zero,
          title: const Text(
            'Вы действительно хотите выйти?',
            textAlign: TextAlign.center,
          ),
          content: Row(
            children: [
              Expanded(
                child: CustomButton(
                  colorBtn: AppColors.yellow,
                  onPress: () {
                    Navigator.pop(context);
                  },
                  text: 'нет',
                ),
              ),
              const SizedBox(width: 12),
              Expanded(
                child: CustomButton(
                  colorBtn: AppColors.red,
                  onPress: () async {
                    await sl<Box<ProfileHiveModel>>().clear();
                    await sl<Box<CountryHiveModel>>().clear();
                    if (context.mounted) {
                      Navigator.pop(context);
                    }
                    AppRouting.pushAndPopUntilFunction(
                        const BottomNavigationRoute());
                  },
                  text: 'Да',
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
