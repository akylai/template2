import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:school/core/functions/push_router_func.dart';
import 'package:school/features/auth/domain/use_cases/auth_use_case.dart';
import 'package:school/features/bottom_navigator/bottom_navigator_cubit/bottom_navigation_cubit.dart';
import 'package:school/routes/mobile_auto_router.gr.dart';
import 'package:school/server/service_locator.dart';
import 'package:school/widgets/styled_toasts.dart';

part 'auth_cubit.freezed.dart';
part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit({required this.useCase}) : super(const AuthState.initial());
  final AuthUseCase useCase;

  final numberController = TextEditingController();
  final passwordController = TextEditingController();

  getAuth() async {
    emit(const AuthState.loading());
    try {
      await useCase.getToken(
        login: numberController.text.replaceAll(' ', '').replaceAll('+', ''),
        password: passwordController.text,
      );
      AppRouting.pushAndPopUntilFunction(const BottomNavigationRoute());
      sl<BottomNavigationCubit>().emitCurrentIndex();
    } catch (e) {
      AppSnackBars.showErrorSnackBar(e.toString());
      emit(AuthState.error(e.toString()));
    }
  }

  @override
  Future<void> close() {
    numberController.dispose();
    passwordController.dispose();
    return super.close();
  }
}
