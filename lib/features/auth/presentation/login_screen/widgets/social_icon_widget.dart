import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school/widgets/app_indicator.dart';

class SocalIconWidget extends StatelessWidget {
  final String icon;
  final Function() onTap;
  final bool isLoading;
  const SocalIconWidget({
    required this.onTap,
    required this.icon,
    this.isLoading = false,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: isLoading ? null : onTap,
      child: Container(
        width: 48,
        height: 48,
        margin: const EdgeInsets.symmetric(horizontal: 3),
        padding: const EdgeInsets.all(13),
        decoration: const BoxDecoration(
          color: Color.fromARGB(255, 238, 238, 238),
          shape: BoxShape.circle,
        ),
        child: isLoading
            ? const AppIndicator()
            : SvgPicture.asset(
                icon,
                height: 18,
                width: 18,
              ),
      ),
    );
  }
}
