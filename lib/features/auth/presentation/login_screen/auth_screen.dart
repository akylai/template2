import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:school/core/images/app_img.dart';
import 'package:school/core/images/app_img_svgs.dart';
import 'package:school/features/auth/presentation/login_screen/cubits/auth_cubit/auth_cubit.dart';
import 'package:school/server/service_locator.dart';
import 'package:school/theme/app_colors.dart';
import 'package:school/theme/app_text_styles.dart';
import 'package:school/widgets/custom_button.dart';
import 'package:school/widgets/custom_text_field.dart';
import 'package:school/widgets/spaces.dart';

@RoutePage()
class AuthScreen extends StatelessWidget {
  const AuthScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => sl<AuthCubit>(),
        ),
      ],
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 24),
                      SvgPicture.asset(
                        AppImgSvgs.test,
                        height: 33,
                      ),
                      const SizedBox(height: 35),
                      Builder(
                        builder: (context) => TextField(
                          controller:
                              context.read<AuthCubit>().numberController,
                        ),
                      ),
                      const SizedBox(height: 12),
                      Builder(
                        builder: (context) => CustomTextField(
                          prefixIconPadding: 10,
                          controller:
                              context.read<AuthCubit>().passwordController,
                          hintText: 'Введите пароль',
                          prefixIcon: const Icon(
                            Icons.security,
                            color: Colors.black,
                            size: 30,
                          ),
                          suffixIcon: true,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () {},
                            child: Text(
                              'забыл',
                              style: AppTextStyles.s15W400(color: Colors.grey),
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                      Builder(
                        builder: (context) => BlocBuilder<AuthCubit, AuthState>(
                          builder: (context, state) {
                            return CustomButton(
                              isLoading: state.isLoading,
                              colorBtn: AppColors.red,
                              onPress: () {
                                context.read<AuthCubit>().getAuth();
                              },
                              text: 'войти',
                            );
                          },
                        ),
                      ),
                      const SizedBox(height: 15),
                      TextButton(
                        onPressed: () {},
                        child: Text(
                          'Зарегистрироваться',
                          style: AppTextStyles.s15W400(color: Colors.grey),
                        ),
                      ),
                      const SizedBox(height: 15),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
