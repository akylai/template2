import 'package:school/features/auth/data/models/auth_model.dart';

abstract class AuthRepo {
  Future<AuthModel> getToken({
    required String login,
    required String password,
  });
}
