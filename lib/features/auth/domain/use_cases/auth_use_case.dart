import 'package:hive/hive.dart';
import 'package:school/core/constants/hive_keys.dart';
import 'package:school/features/auth/data/models/profile_hive_model/profile_hive_model.dart';
import 'package:school/features/auth/domain/repositories/auth_repo.dart';

class AuthUseCase {
  final AuthRepo repo;
  final Box<ProfileHiveModel> profileBox;
  AuthUseCase({
    required this.repo,
    required this.profileBox,
  });

  Future<void> getToken({
    required String login,
    required String password,
  }) async {
    try {
      final result = await repo.getToken(login: login, password: password);
      await profileBox.put(
        HiveKeys.profileBox,
        ProfileHiveModel(
          token: result.token,
          currentUserId: result.currentUserId,
          provider: result.provider,
          role: result.role,
          isPassword: result.isPassword,
          bereshen: result.bereshen,
          name: result.name,
          nickname: result.nickname,
          email: result.email,
        ),
      );
    } catch (e) {
      rethrow;
    }
  }
}
