class AuthModel {
  final String token;
  final int currentUserId;
  final String provider;
  final int role;
  final bool isPassword;
  final int bereshen;
  final String name;
  final String? email;
  final String? nickname;

  AuthModel({
    required this.token,
    required this.currentUserId,
    required this.provider,
    required this.role,
    required this.isPassword,
    required this.bereshen,
    required this.name,
    this.email,
    this.nickname,
  });

  factory AuthModel.fromJson(Map<String, dynamic> json) => AuthModel(
        token: json["token"],
        currentUserId: json["currentUserId"],
        provider: json["provider"],
        role: json["role"],
        name: json["name"],
        bereshen: json["bereshen"] ?? 0,
        isPassword: !json["isPwEmpty"],
        email: json["email"],
        nickname: json["nickname"],
      );
}
