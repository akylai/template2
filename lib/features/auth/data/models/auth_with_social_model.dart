class SocialAuthModel {
  final String name;
  final String email;
  final String provider;
  final String providerId;
  final String providerToken;
  SocialAuthModel({
    required this.name,
    required this.email,
    required this.provider,
    required this.providerId,
    required this.providerToken,
  });

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'name': name,
      'email': email,
      'provider': provider,
      'providerId': providerId,
      'providerToken': providerToken,
    };
  }
}
