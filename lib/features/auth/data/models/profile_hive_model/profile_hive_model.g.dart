// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_hive_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ProfileHiveModelAdapter extends TypeAdapter<ProfileHiveModel> {
  @override
  final int typeId = 1;

  @override
  ProfileHiveModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ProfileHiveModel(
      token: fields[0] as String,
      currentUserId: fields[1] as int,
      provider: fields[2] as String,
      role: fields[3] as int,
      isPassword: fields[4] as bool,
      bereshen: fields[5] as int,
      name: fields[6] as String,
      nickname: fields[7] as String?,
      email: fields[8] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, ProfileHiveModel obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.token)
      ..writeByte(1)
      ..write(obj.currentUserId)
      ..writeByte(2)
      ..write(obj.provider)
      ..writeByte(3)
      ..write(obj.role)
      ..writeByte(4)
      ..write(obj.isPassword)
      ..writeByte(5)
      ..write(obj.bereshen)
      ..writeByte(6)
      ..write(obj.name)
      ..writeByte(7)
      ..write(obj.nickname)
      ..writeByte(8)
      ..write(obj.email);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProfileHiveModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
