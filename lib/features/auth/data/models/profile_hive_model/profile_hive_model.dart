import 'package:hive_flutter/hive_flutter.dart';

part 'profile_hive_model.g.dart';

@HiveType(typeId: 1)
class ProfileHiveModel extends HiveObject {
  @HiveField(0)
  String token;

  @HiveField(1)
  int currentUserId;

  @HiveField(2)
  String provider;

  @HiveField(3)
  int role;

  @HiveField(4)
  bool isPassword;

  @HiveField(5)
  int bereshen;

  @HiveField(6)
  String name;

  @HiveField(7)
  String? nickname;

  @HiveField(8)
  String? email;

  ProfileHiveModel({
    required this.token,
    required this.currentUserId,
    required this.provider,
    required this.role,
    required this.isPassword,
    required this.bereshen,
    required this.name,
    this.nickname,
    this.email,
  });
}
