import 'package:dio/dio.dart';
import 'package:school/features/auth/data/models/auth_model.dart';
import 'package:school/features/auth/domain/repositories/auth_repo.dart';
import 'package:school/server/catch_exception.dart';

class AuthRepoImpl implements AuthRepo {
  final Dio dio;
  AuthRepoImpl({required this.dio});

  @override
  Future<AuthModel> getToken({
    required String login,
    required String password,
  }) async {
    try {
      final response = await dio.post(
        'login',
        data: {
          'phone': login,
          'password': password,
        },
      );
      return AuthModel.fromJson(response.data);
    } catch (e) {
      throw CatchException.convertException(e).message;
    }
  }
}
