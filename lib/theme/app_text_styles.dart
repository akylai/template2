import 'package:flutter/material.dart';

abstract class AppTextStyles {
  static TextStyle s11W400({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 11,
        fontWeight: FontWeight.w400,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s12W400({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 12,
        fontWeight: FontWeight.w400,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s13W400({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 13,
        fontWeight: FontWeight.w400,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s14W400({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 14,
        fontWeight: FontWeight.w400,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s15W400({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 15,
        fontWeight: FontWeight.w400,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s15W500({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 15,
        fontWeight: FontWeight.w500,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s15W600({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 15,
        fontWeight: FontWeight.w600,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s15W700({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 15,
        fontWeight: FontWeight.w700,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s16W400({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s16W500({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 16,
        fontWeight: FontWeight.w500,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s16W600({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 16,
        fontWeight: FontWeight.w600,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s17W600({Color? color, double? height}) => TextStyle(
        fontSize: 17,
        fontWeight: FontWeight.w600,
        color: color ?? Colors.black,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s17W700({Color? color, double? height}) => TextStyle(
        color: color ?? Colors.black,
        fontSize: 17,
        fontWeight: FontWeight.w700,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s18W700({Color? color, double? height}) => TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.w700,
        color: color ?? Colors.black,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s20W700({Color? color, double? height}) => TextStyle(
        fontSize: 20,
        color: Colors.black,
        fontWeight: FontWeight.w700,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s24W700({Color? color, double? height}) => TextStyle(
        fontSize: 24,
        color: color ?? Colors.black,
        fontWeight: FontWeight.w700,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s24W900({Color? color, double? height}) => TextStyle(
        fontSize: 24,
        color: color ?? Colors.black,
        fontWeight: FontWeight.w900,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
  static TextStyle s28W900({Color? color, double? height}) => TextStyle(
        fontSize: 28,
        color: Colors.black,
        fontWeight: FontWeight.w900,
        fontFamily: 'NotoSans',
        height: height ?? 1,
      );
}
