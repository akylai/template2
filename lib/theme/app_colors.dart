import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color red = Color(0xffff0057);
  static const Color yellow = Color.fromRGBO(250, 169, 85, 1);
  static const Color pinkLight = Color(0xffFFEFF0);

  static const Color blackIcon = Color(0xff444555);
  static const Color bgGrey = Color(0xfff3f4f6);
  static const Color bgGrey2 = Color(0xfff0f0f0);
  static const Color bgGrey3 = Color(0xffa2a2a2);
  static const Color greyT6 = Color(0xff666666);

  static const Color blackBg = Color(0xff01061C);
  static const Color cappuchino = Color(0xffE1D5C9);

  static const Color blue = Color(0xff274da2);
  static const Color peach = Color(0xffFFBE98);
  static const Color peach2 = Color(0xffFFDBC9);
  static const Color peachLight = Color(0xffFBF2E9);
  static const Color lemon = Color(0xffFDD878);
  static const Color mint = Color(0xff00A170);
  static const Color marlin = Color(0xffBCAFCF);
  static const Color bej = Color(0xffFFF8F7);
}
