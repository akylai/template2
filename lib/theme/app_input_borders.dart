import 'package:flutter/material.dart';
import 'package:school/theme/app_colors.dart';

class AppInputBorders {
  static OutlineInputBorder bgGrey2 = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: AppColors.bgGrey2,
      width: 1.5,
    ),
  );
  static OutlineInputBorder bgGrey3 = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: AppColors.pinkLight,
      width: 1.5,
    ),
  );
  static const OutlineInputBorder outlineBorderColorGreenWidth2Radius10 =
      OutlineInputBorder(
    borderSide: BorderSide(
      color: AppColors.yellow,
      width: 1.5,
    ),
    borderRadius: BorderRadius.all(
      Radius.circular(10),
    ),
  );
  static const OutlineInputBorder outlineBordercolorF68080Width2Radius10 =
      OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.green,
      width: 1.5,
    ),
    borderRadius: BorderRadius.all(
      Radius.circular(
        10,
      ),
    ),
  );

  static const UnderlineInputBorder unOutlineBorderColorE5E5E5Width1 =
      UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.pinkAccent,
      width: 1,
    ),
  );
  static const UnderlineInputBorder unOutlineBorderColorGreenWidth2 =
      UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.black,
      width: 1,
    ),
  );
  static const UnderlineInputBorder unOutlineBordercolorF68080Width2 =
      UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.purple,
      width: 1,
    ),
  );
}
