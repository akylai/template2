class AppValidators {
  static String? emptyValidator(String? val) {
    if (val == null || val.isEmpty) {
      return 'Поле обязательно для заполнения'; //LocaleKeys.requiredField.tr() TODO
    }
    return null;
  }

  static String? passwordValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Поле обязательно для заполнения';
    }

    if (value.length < 8) {
      return 'Пароль должен содержать не менее 8 символов';
    }

    if (!RegExp(r'[a-zA-Zа-яА-Я]').hasMatch(value) ||
        !RegExp(r'[0-9]').hasMatch(value)) {
      return 'Пароль должен содержать буквы и цифры';
    }

    return null;
  }
}
