import 'package:intl/intl.dart';

class DateFormats {
  static final DateFormat formatDDmmYYYY = DateFormat('dd.MM.yyyy');
}
