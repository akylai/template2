// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'county_hive_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CountryHiveModelAdapter extends TypeAdapter<CountryHiveModel> {
  @override
  final int typeId = 0;

  @override
  CountryHiveModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CountryHiveModel(
      countyName: fields[0] as String,
      countyId: fields[1] as int,
      cityName: fields[2] as String,
      cityId: fields[3] as int,
    );
  }

  @override
  void write(BinaryWriter writer, CountryHiveModel obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.countyName)
      ..writeByte(1)
      ..write(obj.countyId)
      ..writeByte(2)
      ..write(obj.cityName)
      ..writeByte(3)
      ..write(obj.cityId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CountryHiveModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
