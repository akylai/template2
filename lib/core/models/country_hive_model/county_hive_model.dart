import 'package:hive_flutter/hive_flutter.dart';

part 'county_hive_model.g.dart';

@HiveType(typeId: 0)
class CountryHiveModel extends HiveObject {
  @HiveField(0)
  String countyName;

  @HiveField(1)
  int countyId;

  @HiveField(2)
  String cityName;

  @HiveField(3)
  int cityId;  

  CountryHiveModel({
    required this.countyName,
    required this.countyId,
    required this.cityName,
    required this.cityId,
  });
}
