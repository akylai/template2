class DictionaryModel {
  final int id;
  final String value;
  const DictionaryModel({
    required this.id,
    required this.value,
  });

  @override
  bool operator ==(covariant DictionaryModel other) {
    if (identical(this, other)) return true;

    return other.value == value && other.id == id;
  }

  @override
  int get hashCode => value.hashCode ^ id.hashCode;

  factory DictionaryModel.fromJson(Map<String, dynamic> map) {
    return DictionaryModel(
      id: map['id'] as int,
      value: map['value'] as String,
    );
  }
}
