class CountryModel {
  final String countryName;
  final int countryId;
  final List<CityModel> cities;
  CountryModel({
    required this.countryName,
    required this.countryId,
    required this.cities,
  });

  factory CountryModel.fromJson(Map<String, dynamic> map) {
    return CountryModel(
      countryName: map['countryName'] as String,
      countryId: map['countryId'] as int,
      cities:
          map['cities'].map<CityModel>((e) => CityModel.fromJson(e)).toList(),
    );
  }
}

class CityModel {
  final String cityName;
  final int cityId;
  CityModel({
    required this.cityName,
    required this.cityId,
  });

  factory CityModel.fromJson(Map<String, dynamic> map) {
    return CityModel(
      cityName: map['cityName'] as String,
      cityId: map['cityId'] as int,
    );
  }
}
