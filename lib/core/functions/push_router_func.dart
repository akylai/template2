import 'package:auto_route/auto_route.dart';
import 'package:school/routes/mobile_auto_router.dart';
import 'package:school/server/service_locator.dart';

class AppRouting {
  static pushFunction(PageRouteInfo<dynamic> route) async {
    await sl<AppRouter>().push(route);
  }

  static pushAndPopUntilFunction(PageRouteInfo<dynamic> route) {
    sl<AppRouter>().pushAndPopUntil(
      route,
      predicate: (route) => false,
    );
  }

  static popFunction({dynamic result}) {
    sl<AppRouter>().pop(result);
  }
}
