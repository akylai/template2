class AppImgSvgs {
  AppImgSvgs._();

  static const String bgWhiteCircle = 'assets/svgs/bgWhiteCircle.svg';
  static const String bgWhiteCircleFinish =
      'assets/svgs/bgWhiteCircleFinish.svg';
  static const String bgWhiteCircleStart = 'assets/svgs/bgWhiteCircleStart.svg';
  static const String chart = 'assets/svgs/chart.svg';
  static const String chartOutline = 'assets/svgs/chart_outline.svg';
  static const String home = 'assets/svgs/home.svg';
  static const String homeOutline = 'assets/svgs/home_outline.svg';
  static const String profile = 'assets/svgs/profile.svg';
  static const String profileOutline = 'assets/svgs/profile_outline.svg';
  static const String test = 'assets/svgs/test.svg';
  static const String testOutline = 'assets/svgs/test_outline.svg';
}
