import 'package:flutter/material.dart';
import 'package:school/core/models/dictionary_model.dart';
import 'package:school/theme/app_text_styles.dart';

class CustomDropdown extends StatelessWidget {
  final String label;
  final List<DictionaryModel> items;
  const CustomDropdown({super.key, required this.label, required this.items});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 16, right: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<DictionaryModel>(
          padding: const EdgeInsets.symmetric(vertical: 3),
          borderRadius: BorderRadius.circular(10),
          dropdownColor: Colors.white,
          isExpanded: true,
          hint: Text(
            label,
            style: AppTextStyles.s15W400(),
          ),
          items: items
              .map(
                (e) => DropdownMenuItem(
                  value: e,
                  child: Text(
                    e.value,
                    style: AppTextStyles.s15W400(),
                  ),
                ),
              )
              .toList(),
          onChanged: (DictionaryModel? clas) {
            // context.read<PostFoodCubit>().getUnitId(unit!);
          },
        ),
      ),
    );
  }
}
