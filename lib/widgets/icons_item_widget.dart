import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:school/core/images/app_img_svgs.dart';
import 'package:school/theme/app_colors.dart';
import 'package:school/theme/app_text_styles.dart';

class IconsItemWidget extends StatelessWidget {
  final String title;
  final Color color;
  final bool? isStart;
  final bool? isFinish;
  final Function() onTap;
  const IconsItemWidget({
    super.key,
    required this.title,
    required this.onTap,
    required this.color,
    this.isStart,
    this.isFinish,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onTap,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                SvgPicture.asset(
                  isStart == true
                      ? AppImgSvgs.bgWhiteCircleStart
                      : isFinish == true
                          ? AppImgSvgs.bgWhiteCircleFinish
                          : AppImgSvgs.bgWhiteCircle,
                ),
                Positioned(
                  top: 4,
                  left: isStart == true
                      ? 4
                      : isFinish == true
                          ? 11.2
                          : 12,
                  child: CircleAvatar(
                    radius: 24,
                    backgroundColor: color,
                    child: LayoutBuilder(
                      builder: (context, constraint) {
                        return SvgPicture.asset(
                          AppImgSvgs.test,
                          colorFilter: const ColorFilter.mode(
                            Colors.white,
                            BlendMode.srcIn,
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 8),
            SizedBox(
              width: isStart == true
                  ? 56
                  : isFinish == true
                      ? 68
                      : 70,
              child: Text(
                title,
                style: AppTextStyles.s12W400(
                  color: AppColors.blackBg,
                  height: 1.2,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
