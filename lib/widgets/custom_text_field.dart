import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:school/theme/app_colors.dart';
import 'package:school/theme/app_input_borders.dart';
import 'package:school/theme/app_text_styles.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField({
    super.key,
    required this.hintText,
    this.prefixIcon,
    required this.controller,
    this.suffixIcon = false,
    this.prefixIconPadding = 0,
    this.validator,
    this.keyboardType,
    this.inputFormatters,
    this.onChanged,
    this.minLines = 1,
    this.maxLines = 1,
  });
  final String hintText;
  final Widget? prefixIcon;
  final TextEditingController controller;
  final bool suffixIcon;
  final double prefixIconPadding;
  final String? Function(String?)? validator;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final Function(String)? onChanged;

  final int minLines;
  final int maxLines;

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool _isVisible = false;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      minLines: widget.minLines,
      maxLines: widget.maxLines,
      onChanged: (value) {
        if (widget.onChanged != null) {
          widget.onChanged!(value);
        }
      },
      validator: widget.validator,
      inputFormatters: widget.inputFormatters,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: widget.controller,
      obscureText: widget.suffixIcon ? !_isVisible : false,
      keyboardType: widget.keyboardType,
      decoration: InputDecoration(
        fillColor: Colors.white,
        filled: true,
        prefixIcon: Padding(
          padding: EdgeInsets.all(widget.prefixIconPadding),
          child: widget.prefixIcon,
        ),
        // prefixIconColor: AppColors.blueDark, //глазик
        suffixIcon: widget.suffixIcon
            ? IconButton(
                onPressed: () {
                  setState(() {
                    _isVisible = !_isVisible;
                  });
                },
                icon: Icon(
                  _isVisible ? Icons.visibility : Icons.visibility_off,
                  // color: AppColors.blueDark, //глазик
                ),
              )
            : null,
        hintText: widget.hintText,
        hintStyle: AppTextStyles.s14W400(color: AppColors.bgGrey3),
        border: AppInputBorders.bgGrey2,
        enabledBorder: AppInputBorders.bgGrey2,
        focusedBorder: AppInputBorders.bgGrey3,
        contentPadding: const EdgeInsets.symmetric(vertical: 15),
        isDense: true,
      ),
    );
  }
}
