import 'package:flutter/material.dart';
import 'package:school/theme/app_text_styles.dart';

class NoDataWidget extends StatelessWidget {
  const NoDataWidget({
    this.title = '',
    required this.image,
    super.key,
    required this.imageHeight,
  });
  final String title;
  final String image;
  final double imageHeight;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(image, height: imageHeight),
          const SizedBox(height: 20),
          Text(
            title == '' ? 'no data' : title,
            textAlign: TextAlign.center,
            style: AppTextStyles.s16W500(
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
