import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:school/theme/app_colors.dart';
import 'package:school/theme/app_text_styles.dart';
import 'package:school/widgets/app_indicator.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Function() onPress;
  final Widget? child;
  final double? width;
  final double height;
  final double contentPadding;
  final EdgeInsetsGeometry aroundButtonPadding;
  final Color colorBtn;
  final Color textColor;
  final Color? borderColor;
  final bool isFullFilled;
  final double radius;
  final bool isLoading;
  final TextStyle? textStyle;
  const CustomButton({
    super.key,
    required this.text,
    required this.onPress,
    this.child,
    this.colorBtn = AppColors.blue,
    this.width = double.infinity,
    this.height = 55,
    this.isFullFilled = true,
    this.isLoading = false,
    this.contentPadding = 12.0,
    this.aroundButtonPadding = EdgeInsets.zero,
    this.radius = 12.0,
    this.textStyle,
    this.borderColor,
    this.textColor = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      padding: EdgeInsets.zero,
      onPressed: isLoading ? null : onPress,
      child: Container(
        alignment: Alignment.center,
        height: height,
        width: double.infinity,
        padding: EdgeInsets.all(contentPadding),
        decoration: BoxDecoration(
          color: colorBtn,
          borderRadius: BorderRadius.circular(radius),
          border: borderColor != null
              ? Border.all(
                  color: borderColor!,
                )
              : null,
        ),
        child: isLoading
            ? const AppIndicator(color: Colors.white)
            : child ??
                Text(
                  text,
                  textAlign: TextAlign.center,
                  style: textStyle ??
                      AppTextStyles.s15W400(
                        color: textColor,
                      ),
                ),
      ),
    );
  }
}
