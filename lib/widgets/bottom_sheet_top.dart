import 'package:flutter/material.dart';

class BottomSheetTop extends StatelessWidget {
  const BottomSheetTop({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          const SizedBox(height: 12),
          Container(
            height: 5,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(50),
            ),
          ),
          const SizedBox(height: 12),
        ],
      ),
    );
  }
}
