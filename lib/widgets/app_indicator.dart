import 'package:flutter/material.dart';
import 'package:school/theme/app_colors.dart';

class AppIndicator extends StatelessWidget {
  const AppIndicator({
    super.key,
    this.color = AppColors.red,
    this.strokeWidth = 2.5,
    this.radius = 24,
  });
  final Color? color;
  final double strokeWidth;
  final double radius;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox.square(
        dimension: radius,
        child: CircularProgressIndicator(
          color: color,
          strokeWidth: strokeWidth,
        ),
      ),
    );
  }
}
