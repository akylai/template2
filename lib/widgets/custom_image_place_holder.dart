import 'package:flutter/material.dart';
import 'package:school/widgets/spaces.dart';
import 'package:shimmer/shimmer.dart';

class CustomImagePlaceHolder extends StatelessWidget {
  const CustomImagePlaceHolder({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      width: context.width,
      child: Shimmer.fromColors(
        baseColor: Colors.grey.withOpacity(0.4),
        highlightColor: Colors.white,
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.grey,
          ),
        ),
      ),
    );
  }
}
