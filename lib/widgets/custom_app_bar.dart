import 'package:flutter/material.dart';
import 'package:school/theme/app_text_styles.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    this.title = '',
    this.icon,
    this.titleWidget,
    this.centerTitle = false,
    this.actions,
    super.key,
  });
  final String title;
  final Widget? icon;
  final Widget? titleWidget;
  final bool centerTitle;
  final List<Widget>? actions;
  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      title: titleWidget ??
          Text(
            title,
            maxLines: 2,
            textAlign: TextAlign.center,
          ),
      titleTextStyle: AppTextStyles.s20W700(),
      elevation: 0,
      centerTitle: centerTitle,
      iconTheme: const IconThemeData(
        color: Colors.black,
      ),
      leading: icon,
      actions: actions,
    );
  }
}
